<#
.SYNOPSIS
Get deployment status for a Source and Destination Environments.
.DESCRIPTION
Get deployment status for a Source and Destination Environments.
.PARAMETER ProjectName
The name of a project in Octopus Deploy.
.PARAMETER SourceEnv
The name of a SourceEnvironment in Octopus Deploy.
.PARAMETER DestEnv
The name of a DestEnvironment in Octopus Deploy.
.PARAMETER Environment
The name of a environment in Octopus Deploy.
.PARAMETER BaseUrl
Base URL of the Octopus Deploy server.
.PARAMETER ApiKey
The API key to use when authenticating.
.PARAMETER OctoExePath
The absolut path to the Octo.exe executable.
.PARAMETER webhookUri
Uri for the MS Teams.
.EXAMPLE
DeploymentPromotionCheck.ps1 -ProjectName MyProjectName -Environment MyEnvironmentName .......
.LINK
https://octopus.com/docs/api-and-integration/octo.exe-command-line
#>
Param(
    [Parameter(Mandatory = $true, Position = 1)][string]$ProjectName,
    [Parameter(Mandatory = $true, Position = 2)][string]$SourceEnv,
	[Parameter(Mandatory = $true, Position = 3)][string]$DestEnv,
    [Parameter(Mandatory = $true, Position = 4)][string]$BaseUrl,
    [Parameter(Mandatory = $true, Position = 5)][string]$ApiKey,
    [Parameter(Mandatory = $true, Position = 6)][string]$OctoExePath,
	[Parameter(Mandatory = $true, Position = 7)][string]$webhookUri
)


# Get deploy states
$SourceEnvDeployDetails = .\Get-LatestDeployment.ps1 -ProjectName $projectName -Environment $SourceEnv -BaseUrl $octopusBaseUrl -Apikey $ApiKey -OctoExePath $octoExePath
$DestEnvDeployDetails = .\Get-LatestDeployment.ps1 -ProjectName $projectName -Environment $DestEnv -BaseUrl $octopusBaseUrl -Apikey $ApiKey -OctoExePath $octoExePath

# Extract build numbers
$SrcBuildNum = $SourceEnvDeployDetails.Version.Split('.')[2] -as [int];
$DestBuildNum = $DestEnvDeployDetails.Version.Split('.')[2] -as [int];

#Checking Deployment Status
if ($SourceEnvDeployDetails.State -eq "Success")
{
if ($SrcBuildNum -gt $DestBuildNum)
{
  Write-Host "Deploy current release in $SourceEnv to $DestEnv"
  Continue
}
else
{
  Write-Host "Don't deploy $SourceEnv to $DestEnv as $DestEnv is already holding latest version."
  $messageBody =
@"
{
    "@type": "MessageCard",
    "@context": "https://schema.org/extensions",
    "summary": "Octopus Deploy Notification - PLES",
    "themeColor": "0078D7",
    "title": "[BO Plan] No New release available in $SourceEnv. Hence Skipped deployment in $DestEnv."
}
"@
  .\Start-TeamsNotification -WebhookUri $webhookUri -MessageBody $messageBody
  Exit 1
}
}
else
{
  Write-Host "Deployment in $SourceEnv is in Non-successful state. Don't deploy."
  $messageBody =
@"
{
    "@type": "MessageCard",
    "@context": "https://schema.org/extensions",
    "summary": "Octopus Deploy Notification - PLES",
    "themeColor": "d70000",
    "title": "[BO Plan] Deployment in $SourceEnv is in Non-Successful state. Hence skipped deploy to $DestEnv."
}
"@
  .\Start-TeamsNotification -WebhookUri $webhookUri -MessageBody $messageBody
  Exit 1
}



  